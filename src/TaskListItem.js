import { View, StyleSheet, TextInput, Text, TouchableOpacity } from "react-native";
import { useRef, useState } from "react";
import { API_URL } from "./config";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Swipeable } from "react-native-gesture-handler";
import SwipeDeleteButton from "./SwipeDeleteButton";
import SwipeButton from "./SwipeButton";
import Icon from "react-native-vector-icons/MaterialIcons";

export default function(props){

  const [loading,setLoading] = useState(false)
  const loadingRef = useRef(false)
  const [task,setTask] = useState(props.item ? props.item : {})
  const [text,setText] = useState(props.item.name)

  async function save(status){

    if(loadingRef.current || text == ""){
      return
    }

    loadingRef.current = true
    setLoading(true)

    let projectId = await AsyncStorage.getItem('projectId');

    // console.log('projectId',projectId)

    fetch(API_URL + 'task' + (task.isNew ? "" : `/${task._id}`), {
      method: task.isNew ? 'POST' : 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({name: text,status, projectId}),
    })
      .then(response => response.json())
      .then(json => {

        loadingRef.current = false
        setLoading(false)

        // console.log('json task',json.task)

        if(json.task){
          setTask(json.task)
        }

        if(!projectId){

          AsyncStorage.setItem('projectId',json.task.projectId)
        }

      })
      .catch(error => {

        console.log('error',error)

        loadingRef.current = false
        setLoading(false)
      });

  }

  function setStatus(){

    if(task.status == "done"){
      setTask({...task,status: "created"})
      save("created")
    }else{
      setTask({...task,status: "done"})
      save("done")
    }

  }

  return<Swipeable
    renderLeftActions= {()=><SwipeButton style = {styles.button} onPress = {setStatus} ><Icon name = {task && task.status == "done" ? "radio-button-checked" : "radio-button-unchecked"}  size={20} color = "#000" /></SwipeButton>}
    renderRightActions= {()=><SwipeDeleteButton style = {styles.deleteButton} onPress = {()=>props.onDelete(task)} >Устгах</SwipeDeleteButton>} >
    <TouchableOpacity
      style={[styles.container,{marginBottom: props.last ? 80 : 8}]}
      onLongPress={props.drag}
      disabled={props.isActive}
    >
      <TextInput
        style={[styles.text,{textDecorationLine: task.status == "done" ? "line-through" : "none"}]}
        value={text}
        autoFocus = {task ? task.isNew : false}
        returnKeyType = "done"
        onSubmitEditing={save}
        blurOnSubmit={true}
        onChangeText={(text)=>setText(text)}
        onBlur={save}
      />
    </TouchableOpacity>
  </Swipeable>
}

const styles = StyleSheet.create({
  container:{
    margin: 8,
    padding: 16,
    backgroundColor: "#fff",
    borderRadius: 12,
  },

  text:{
    color: "#000",
    fontSize: 16,
  },

  deleteButton:{
    borderRadius: 12,
    marginTop: 8,
    marginBottom: 8
  }
})


