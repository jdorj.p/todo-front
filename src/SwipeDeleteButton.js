import SwipeButton from "./SwipeButton";
import { StyleSheet } from "react-native";

export default function(props){
  return <SwipeButton {...props} style = {[styles.background,props.style]} textStyle = {styles.text} />
}

const styles = StyleSheet.create({

  background: {
    backgroundColor: "#DF2D43"
  },

  text:{
    color: "#fff"
  }

})
