import { Text, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

export default function(props){

  return <TouchableOpacity style={styles.container} onPress={props.onPress}>
    <Icon name = "add" size={32} color = "#fff" />
  </TouchableOpacity>
}

const styles = StyleSheet.create({

  container:{
    width: 56,
    height: 56,
    borderRadius:28,
    backgroundColor: "#00AAFF",
    color: "#fff",
    justifyContent: "center",
    alignItems: "center",
  },
})
