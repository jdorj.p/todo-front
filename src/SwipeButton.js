import { StyleSheet, Text, TouchableOpacity } from "react-native";

export default function(props){

  return <TouchableOpacity onPress={props.onPress} style = {[styles.touchableOpacity,props.style]} ><Text style = {[styles.text,props.textStyle]} >{props.children}</Text></TouchableOpacity>
}

const styles = StyleSheet.create({

  touchableOpacity: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8
  },

  text: {
    color: '#000',
    fontSize: 14,
    fontFamily: 'Roboto-Bold',
  },

})
