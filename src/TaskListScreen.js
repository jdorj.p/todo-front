import { SafeAreaView, StyleSheet, View, RefreshControl, KeyboardAvoidingView, Platform, Text } from "react-native";
import DraggableFlatList from 'react-native-draggable-flatlist'
import { useEffect, useRef, useState } from "react";
import { API_URL } from "./config";
import TaskListItem from "./TaskListItem";
import FloatingButton from "./FloatingButton";
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { GestureHandlerRootView } from "react-native-gesture-handler";
const statusBarHeight = getStatusBarHeight(true)
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function(){

  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)
  const newIndex = useRef(0)

  function add(){

    setData([...data,{isNew: true, _id:`new${newIndex.current}`,name: ""}])
    newIndex.current += 1
  }

  useEffect(()=>{
    getData()
  },[])

  async function getData(){

    let projectId = await AsyncStorage.getItem('projectId');

    if(!projectId){
      return
    }

    setLoading(true)

    fetch(API_URL + 'project/' + projectId, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(json => {

        setLoading(false)
        setData(json.tasks)

      })
      .catch(error => {

        setLoading(false)
      });
  };

  async function saveData(data){

    let projectId = await AsyncStorage.getItem('projectId');

    setLoading(true)

    fetch(API_URL + 'project/' + projectId, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({tasks: data.map(i=>i._id)}),
    })
      .then(response => response.json())
      .then(json => {

        setLoading(false)

      })
      .catch(error => {

        setLoading(false)
      });
  };

  function saveOrder(data){

    setData(data)
    saveData(data)
  }

  function deleteTask(task){

    if(!task.isNew){

      fetch(API_URL + 'task/' + task._id, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then(response => response.json())
        .then(json => {

          console.log('delete',json)

        })
        .catch(error => {

        });

    }

    setData(data.filter(item=>item._id != task._id))
  }

  return <SafeAreaView style = {styles.safeAreaView} >

    <Text style = {styles.title} >To Do List</Text>

    <GestureHandlerRootView style = {styles.container} >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style = {styles.keyboardAvoidingView}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 + statusBarHeight : 0}
      >
        <DraggableFlatList
          style={styles.flatList}
          data={data}
          onDragEnd={({ data }) => saveOrder(data)}
          renderItem={({item,index, drag, isActive}) =>
            <TaskListItem
              item = {item}
              drag = {drag}
              isActive = {isActive}
              last = {(data.length - 1) == index}
              onDelete = {deleteTask}
            />}
          keyExtractor={(item) => item._id}
          refreshControl={<RefreshControl refreshing={loading} onRefresh={getData} />}
        />
      </KeyboardAvoidingView>
      <View style = {styles.buttonContainer} >
        <FloatingButton onPress = {add} />
      </View>
    </GestureHandlerRootView>
  </SafeAreaView>
}


const styles = StyleSheet.create({

  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginLeft: 16,
  },

  safeAreaView:{
    flex:1,
    backgroundColor: "#eee"
  },

  container:{
    flex:1,
  },

  keyboardAvoidingView:{
    flex:1,
  },

  flatList: {
    padding: 8,
    overflow: "visible"
  },

  buttonContainer:{
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
  },


});
